import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class ApiServicesService {

  public endPointCharacters:string ="https://rickandmortyapi.com/api/character";

  constructor(private http: HttpClient) { }

  public getAllCharacters(){
    return this.http.get(this.endPointCharacters);
  }
}
