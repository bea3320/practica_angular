import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'navigation',
    pathMatch: 'full',
  },

  {
    path: 'navigation',
    loadChildren: () =>
      import('./pages/navigation/navigation.module').then(
        (module) => module.NavigationModule
      ),
  },

  {
    path: 'page-one',
    loadChildren: () =>
      import('./pages/page-one/page-one.module').then(
        (module) => module.PageOneModule
      ),
  },

  {
    path: 'page-two',
    loadChildren: () =>
      import('./pages/page-two/page-two.module').then(
        (module) => module.PageTwoModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
