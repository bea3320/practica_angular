import { Component, Input, OnInit, SimpleChange } from '@angular/core';
import { Formulario } from '../models/navigation-form';

@Component({
  selector: 'app-navigation-list',
  templateUrl: './navigation-list.component.html',
  styleUrls: ['./navigation-list.component.scss'],
})
export class NavigationListComponent implements OnInit {
  @Input() public formularioCompleted: Formulario | null = null;
  public navigationList: Array<Formulario | null> = [];

  constructor() {}

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChange) {
    this.newFormulario();
  }

  public newFormulario() {
    this.formularioCompleted != undefined || this.formularioCompleted != null
      ? this.navigationList.push(this.formularioCompleted)
      : console.log('Vacio');
  }
}
