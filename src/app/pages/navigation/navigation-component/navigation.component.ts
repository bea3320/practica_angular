import { Formulario } from './models/navigation-form';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss'],
})
export class NavigationComponent implements OnInit {
  public navigationForm: FormGroup;
  public submitted: boolean = false;

  public formularioCompleted: Formulario | null = null;

  constructor(private formBuilder: FormBuilder) {
    this.navigationForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(1)]],
      age: ['', [Validators.required]],
      ciudad: ['', [Validators.required]],
      email: ['', [Validators.email, Validators.required]],
    });
  }

  ngOnInit(): void {
    /*empty*/
  }

  public onSubmit(): void {
    this.submitted = true;

    if (this.navigationForm?.valid) {
      const FORMULARIO: Formulario = {
        name: this.navigationForm.get('name')?.value,
        age: this.navigationForm.get('age')?.value,
        ciudad: this.navigationForm.get('ciudad')?.value,
        email: this.navigationForm.get('email')?.value,
      };

      this.formularioCompleted = FORMULARIO;
      this.navigationForm?.reset();
      this.submitted = false;
    }
  }
}
