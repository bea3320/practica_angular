import { NavigationRoutingModule } from './navigation-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavigationComponent } from './navigation-component/navigation.component';
import { NavigationListComponent } from './navigation-component/navigation-list/navigation-list.component';

@NgModule({
  declarations: [NavigationComponent, NavigationListComponent],
  imports: [CommonModule, NavigationRoutingModule],
})
export class NavigationModule {}
