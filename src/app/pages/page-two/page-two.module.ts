import { PageTwoRoutingModule } from './page-two-routing.module';
import { PageTwoComponent } from './page-two-component/page-two.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [PageTwoComponent],
  imports: [CommonModule, PageTwoRoutingModule],
})
export class PageTwoModule {}
