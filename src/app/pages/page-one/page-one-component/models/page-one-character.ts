export interface PageOneCharacter {
  id: number,
  name: string,
  species: string,
  gender: string,
  image:string,
}

export interface pageOneCharacterResponse{
  "info": {
    "count": number;
    "pages": number;
    "next": "string";
    "prev": "string";
};

"results": PageOneCharacter[];

}
