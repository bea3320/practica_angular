import { PageOneCharacter } from './models/page-one-character';
import { ApiServicesService } from './../../../services/api-services.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page-one',
  templateUrl: './page-one.component.html',
  styleUrls: ['./page-one.component.scss']
})
export class PageOneComponent implements OnInit {

  public characterList: PageOneCharacter[] = [];

  constructor(public apiServicesService: ApiServicesService) {
    this.myListCharacters();
   }

  ngOnInit(): void { }

  public myListCharacters(){
    this.apiServicesService.getAllCharacters().subscribe((data:any) =>
    {
      const results: PageOneCharacter[] = data.results;
      const formattedResults = results.map(({id, name, species, gender, image})=>({
        id,
        name,
        species,
        gender,
        image
      }));
      return this.characterList = formattedResults;
    })
  }



}
