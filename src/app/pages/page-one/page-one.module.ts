import { PageOneRoutingModule } from './page-one-routing.module';
import { PageOneComponent } from './page-one-component/page-one.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [PageOneComponent],
  imports: [CommonModule, PageOneRoutingModule],
})
export class PageOneModule {}
